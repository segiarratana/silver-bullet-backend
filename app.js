const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const app = express();
const PORT = 5050;
const DB_NAME = 'silver-bullet';
const BACKEND_URI = 'mongodb://localhost:27017/';

const files = require('./APIs/files')
const users = require('./APIs/users')
const threads = require('./APIs/threads')

app.use(bodyParser.urlencoded({ extended: false })) // allow user to send data within the URl
app.use(bodyParser.json()); // allow user to send JSON data

// API Calls
app.use('/files', files)
app.use('/users', users)
app.use('/threads', threads)

app.use(function(req, res, next)
{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.listen(PORT);
console.log("Listening on port " + PORT);
