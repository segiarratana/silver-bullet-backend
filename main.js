console.log("Hello World");
const PORT = 5050;
const BACKEND_URL = 'http://localhost:27017/silver-bullet';

async function checkUser()
{
  const data = {username: 'empty', password: 'empty'}
  data.username = document.getElementById('username').value;
  data.password = document.getElementById('password').value;

  const request = new Request(BACKEND_URL + '/APIs/users',
  {
    method: 'GET',
    headers: {'Accept' : 'application.json','Content-Type' : 'application/json'},
    body: JSON.stringify(data)
  })

  const response = await fetch(request);
  if(response.status === 400)
    console.log("Failure! There was an error")
  else if (response.status === 500)
    console.log("Failure! Server error")

  const result = await response.json();
  console.log("Record Checked Sucessfully");
  console.log(result);

  for(var i = 0; i < result.length; i++)
  {
    if(result[i].username == data.username && result[i].password == data.password)
    {
      document.getElementById('LoginMessage').innerHTML += 'Login Successful!';
      fetchFiles();
      fetchThreads();
    }
    else
      document.getElementById('LoginMessage').innerHTML += 'User not found, please create one.';
  }
}

async function postUser()
{
  const data = {newUser: 'Empty', newPass: 'Empty'}
  data.newUser = document.getElementById('newUser').value;
  data.newPass = document.getElementById('newPass').value;

  if(data.newPass == 'Empty' || data.newUser == 'Empty')
  {
    document.getElementById('LoginMessage').innerHTML += 'Must enter new user and pass';
    return;
  }

  console.log(data);
  const request = new Request(BACKEND_URL + '/APIs/users', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
  if(response.status === 400)
    console.log("Failure! There was a 400 error")
  else if (response.status === 500)
  {
    document.getElementById('LoginMessage').innerHTML += 'Server-Side Error: 500';
    console.log("Failure! Server error")
  }
  const result = await response.json();
  console.log("Record inserted successfully");
  console.log(result);
}

async function fetchFiles()
{
  const request = new Request(BACKEND_URL + '/APIs/files', {method: 'GET'});
  const response = await fetch(request);
  const result = await response.json();

  let htmlCode = '';
  htmlCode += '<div class=row>';

  for(var i = 0; i < result.length; i++)
  {
    htmlCode += '<div class="col-md-6" align="center">';
    htmlCode += result[i].fileName; + '</div>';
    htmlCode += '<div class="col-md-3" align="center">';
    htmlCode += result[i].fileSize;
    htmlCode += '<form method="get" action="' + result[i].filePath + '">';
    htmlCode += '<button type="submit">Download!</button></div>';
    htmlCode += '</form>';
    htmlCode += '</div>';
    if((i+1) % 3 == 0)
    {
      htmlCode += '</div>';
      htmlCode += '<div class="row">';
    }
  }
  document.getElementById('grid').innerHTML += htmlCode;
}

async function fetchThreads()
{
  const request = new Request(BACKEND_URL + '/APIs/threads', {method: 'GET'});
  const response = await fetch(request);
  const result = await response.json();

  let htmlCode = '';
  htmlCode += '<div class=col-md-9>';

  for(var i = 0; i < result.length; i++)
  {
    htmlCode += '<div class="row">';
    htmlCode += '<div class="col-md-3" align="center">';
    htmlCode += result[i].title + '</div>'
    htmlCode += '<div class="col-md-6" align="center">';
    htmlCode += result[i].bodyText + '</div>';
    htmlCode += '</div>';
  }
  document.getElementById('threadGrid').innerHTML += htmlCode;
}
