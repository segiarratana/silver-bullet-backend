var express = require('express');
var router = express.Router();
const app = express();

const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const PORT = 5050;
const DB_URI = 'mongodb://localhost:27017';
const DB_NAME = 'silver-bullet';
const THREADS_COLLECTION_NAME = "threads";

const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true })

app.get('/threads', function(req, res)
{
  client.connect(function(err, connection)
  {
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(THREADS_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records)
      {
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });
  });
})

app.post('/threads', function(req, res){

  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is requried"});

  // data validation
  if (!req.body.title || !req.body.content || !req.body.comments || !req.body.postDate || !req.body.postedBy)
    res.status(400).send({message: "title, content, comments, postDate, postedBy are requried"});

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(THREADS_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data)
      {
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});

        connection.close();
        return res.status(200).send({message: "Reocrd inserted successfully"});
      });
  });
})

app.put('/threads/:id', function(req, res)
{
  client.connect(function(err, connection)
  {
    if (err)
      return res.status(500).send({error: err});

    const db = connection.db(DB_NAME);
    db.collection(THREADS_COLLECTION_NAME)
      .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
         if (update_err)
          return res.status(500).send({error: update_err, message: "Could not update the record"});

        return res.status(200).send({message: "Update was successful!", data: update_data});
      });
  });
})

module.exports = router
